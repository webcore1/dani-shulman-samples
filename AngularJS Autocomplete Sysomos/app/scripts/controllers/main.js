'use strict';

/**
 * @ngdoc function
 * @name angularJsAutocompleteSysomosApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularJsAutocompleteSysomosApp
 */
angular.module('angularJsAutocompleteSysomosApp')
  .controller('MainCtrl', function ($scope, $http, $window) {


	$scope.userInput = "";
	$scope.choices = [];
	$scope.showChoices = true;
	$scope.toComplete = "";

	//50 Random words
	function grabData(){
		$scope.choices = ["frequentness","unacquiescent","uninjuring","hearten","awardable","observance","hellespontine","brie","bop","unquarrelsome","misplant","patrimonially","defuze","crematorium","northwoods","unstoneable","vesper","prodemocracy","noninstructive","specializing","ungarmented","gout","prerational","gelene","sheria","legator","ozarks","hastily","incus","thermochemically","policlinic","succubus","hellenism","learnedly","galesburg","disclose","hanger","nagual","bowknot","multisonorous","tenter","nonprosaic","mailer","footing","bernese","promptly","hyperopia","ira","nymphomania","interstriving"];
	}

	//helper function for filter to start from first character
	$scope.customComparator = function(actual, expected){
		console.log(actual, expected);
		if(actual !== expected){
			return (actual.toLowerCase().indexOf(expected.toLowerCase()) === 0);
		}
	}

	//split input to array
	function split(){
		return $scope.userInput.split(" ");
	}

	//when a choice is clicked do
	$scope.select = function(input){

		//clear choices to empty choices from view
		$scope.choices = [];

		//replace the new selection with what user was typing last
		var splitInput = split();
		splitInput[splitInput.length-1] = input;
		$scope.userInput = splitInput.join(" ");


		//Focus form to keep going
		var textarea = $window.document.getElementById('textarea');
		textarea.focus() || null;
	

		//$scope.showChoices = false;
	}

	//listen to changes in the textarea start 
	$scope.onChange = function(input) {
		grabData();	
		var splitInput = split();
		$scope.toComplete = splitInput[splitInput.length-1];
	};
  });
