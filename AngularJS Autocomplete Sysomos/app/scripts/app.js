'use strict';

/**
 * @ngdoc overview
 * @name angularJsAutocompleteSysomosApp
 * @description
 * # angularJsAutocompleteSysomosApp
 *
 * Main module of the application.
 */
angular
  .module('angularJsAutocompleteSysomosApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
.config(function ($routeProvider,$locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
