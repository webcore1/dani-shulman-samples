'use strict';

describe('Service: daniData', function () {

  // load the service's module
  beforeEach(module('angularJsAutocompleteSysomosApp'));

  // instantiate service
  var daniData;
  beforeEach(inject(function (_daniData_) {
    daniData = _daniData_;
  }));

  it('should do something', function () {
    expect(!!daniData).toBe(true);
  });

});
